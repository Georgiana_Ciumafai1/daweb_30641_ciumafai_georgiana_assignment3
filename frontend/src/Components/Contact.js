import * as React from "react";
import "../css/Contact.css";
import { useTranslation } from "react-i18next";
import { Button } from "primereact/components/button/Button";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/components/message/Message";
import { ContactService } from "../Service/ContactService";
import { NotificationManager } from "react-notifications";

function MyComponent() {
  const { t } = useTranslation();
  return <strong>{t("contact.1")}</strong>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <strong>{t("contact.2")}</strong>;
}
function MyComponent3() {
  const { t } = useTranslation();
  return <strong>{t("contact.3")}</strong>;
}
function MyComponent4() {
  const { t } = useTranslation();
  return <strong>{t("contact.4")}</strong>;
}

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formModel: {
        name: "",
        email: "",
        message: "",
      },
      errors: {
        name: "",
        email: "",
        message: "",
      },
      isValid: false,
    };
  }
  submit() {
    let isValid = ContactService.validateForm(
      this.state.formModel,
      this.state.errors
    );

    if (isValid) {
      ContactService.sendMessage(this.state.formModel).then(
        () => {
          NotificationManager.success(
            "Mesajul a fost trimis !",
            "Felicitari !"
          );
          console.log("e ok cred");
          this.setState({ isValid: true });
        },
        (error) => {
          NotificationManager.error("firar");
          this.setState({ isValid: false });
        }
      );
    } else {
      this.setState({ isValid: false });
    }
  }
  render() {
    return (
      <div>
        <div className="wrapper">
          <h3>
            <strong>Contact Form</strong>
          </h3>
          <div className="p-grid p-fluid">
            <div className="p-col-12 p-md-4">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">
                  <i className="pi pi-user"></i>
                </span>
                <InputText
                  placeholder="full name"
                  onChange={(event) => {
                    let formModel = this.state.formModel;
                    formModel.name = event.target.value;
                    this.setState({ formModel: formModel });
                  }}
                />
              </div>
            </div>
            {this.state.errors.name !== "" ? (
              <Message severity="warn" text={this.state.errors.name} />
            ) : null}
            <div className="p-col-12 p-md-4">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">@</span>
                <InputText
                  placeholder="email"
                  onChange={(event) => {
                    let formModel = this.state.formModel;
                    formModel.email = event.target.value;
                    this.setState({ formModel: formModel });
                  }}
                />
                {this.state.errors.email !== "" ? (
                  <Message severity="warn" text={this.state.errors.email} />
                ) : null}
              </div>
            </div>
            <div className="p-col-12 p-md-4">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">M</span>
                <InputText
                  placeholder="Message"
                  onChange={(event) => {
                    let formModel = this.state.formModel;
                    formModel.message = event.target.value;
                    this.setState({ formModel: formModel });
                  }}
                />
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="p-col-12 p-md-4">
            <Button
              label="Send"
              className="p-button-secondary"
              onClick={() => this.submit() & console.log(this.state)}
            />
          </div>
        </div>
        <div className="banner-text">
          <p></p> <hr />
          <h1>Ciumafai Georgiana</h1>
          <hr />
          <p>
            <MyComponent />
          </p>
          <p>
            <MyComponent2 />
          </p>
          <p>
            <MyComponent3 />
          </p>
          <p>
            <MyComponent4 />
          </p>
          <div className="social-links">
            {/* LinkedIn */}
            <a
              href="https://www.linkedin.com/in/georgiana-ciumafai"
              rel="noopener noreferrer"
              target="_blank"
            >
              <i className="fa fa-linkedin-square" aria-hidden="true" />
            </a>

            {/* Github */}
            <a
              href="https://github.com/GeorgianaCiumafai/Problems"
              rel="noopener noreferrer"
              target="_blank"
            >
              <i className="fa fa-github-square" aria-hidden="true" />
            </a>

            {/* Bitbucket */}
            <a
              href="https://bitbucket.org/ciumafai_georgiana/profile/repositories"
              rel="noopener noreferrer"
              target="_blank"
            >
              <i className="fa fa-bitbucket" aria-hidden="true" />
            </a>
          </div>
          <br></br>
          <br></br>
        </div>
        <br></br>
        <br></br>
      </div>
    );
  }
}
export default Contact;
