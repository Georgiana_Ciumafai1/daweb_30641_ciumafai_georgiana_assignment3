import * as React from "react";
import axios from "axios";
import { InputText } from "primereact/components/inputtext/InputText";
import { Button } from "primereact/components/button/Button";
import "../css/Login.css";

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: "",
        password: "",
      },
      email: "",
      password: "",
    };
  }

  handleKeyPress = (event) => {
    //event.preventDefault();
    if (event.key === "Enter") {
      this.submit(event);
    }
  };

  submit(event) {
    event.preventDefault();
    let loginModel = {
      email: this.state.email,
      password: this.state.password,
    };
    axios
      .post("http://localhost:8000/api/login", loginModel, {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      })
      .then((response) => {
        this.setState({ user: response.data });
        sessionStorage.setItem("email", this.state.email);
        console.log(response.data);
        sessionStorage.setItem("name", response.data.name);
        sessionStorage.setItem("id", response.data.id);
        // sessionStorage.setItem("name", response.data.user.name);
        this.props.history.push("/home");
      })
      .catch(() => {
        console.log("Eroare");
      });
  }

  render() {
    //console.log(this.state.user);

    return (
      <React.Fragment>
        {
          <React.Fragment>
            <div className="container">
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4">
                  <InputText
                    placeholder="Email "
                    type="text"
                    size={30}
                    onChange={(event) =>
                      this.setState({ email: event.target.value })
                    }
                  />
                </div>
                <br></br>
                <div className="p-col-12 p-md-4">
                  <InputText
                    placeholder="Password"
                    type="password"
                    onKeyPress={this.handleKeyPress}
                    size={30}
                    onChange={(event) =>
                      this.setState({ password: event.target.value })
                    }
                  />
                </div>
                <div>
                  <Button
                    className="btnn"
                    label="Login"
                    style={{
                      marginTop: `20px`,
                      width: "48%",
                      marginRight: "5%",
                    }}
                    onClick={(e) => {
                      this.submit(e);
                    }}
                  />
                  {
                    <Button
                      className="btnn"
                      label="Sign Up"
                      style={{ marginTop: `5px`, width: "47%" }}
                      onClick={(e) => {
                        this.props.history.push("/register");
                      }}
                    />
                  }
                </div>
              </div>
            </div>
          </React.Fragment>
        }
      </React.Fragment>
    );
  }
}
