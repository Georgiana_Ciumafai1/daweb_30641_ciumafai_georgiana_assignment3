import * as React from "react";
import { Menubar } from "primereact/menubar";
import "../css/Nav.css";
import { useTranslation } from "react-i18next";

function MyComponent() {
  const { t } = useTranslation();
  return <strong>{t("nav.1")}</strong>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <strong>{t("nav.2")}</strong>;
}
function MyComponent3() {
  const { t } = useTranslation();
  return <strong>{t("nav.3")}</strong>;
}
function MyComponent4() {
  const { t } = useTranslation();
  return <strong>{t("nav.4")}</strong>;
}
function MyComponent5() {
  const { t } = useTranslation();
  return <strong>{t("nav.5")}</strong>;
}
function MyComponent6() {
  const { t } = useTranslation();
  return <strong>{t("nav.6")}</strong>;
}
function MyComponent7() {
  const { t } = useTranslation();
  return <strong>{t("nav.7")}</strong>;
}

export class Nav extends React.Component {
  render() {
    return (
      <div className="p-menubar-container">
        <div className="p-menuitem-text">
          <div className="p-menuitem-icon">
            <Menubar
              model={[
                {
                  label: <MyComponent />,
                  icon: "fa fa-home",
                  url: "/home",
                },
                {
                  label: <MyComponent2 />,
                  icon: "fa fa-calendar",
                  url: "/news",
                },
                {
                  label: <MyComponent3 />,
                  icon: "fa fa-file",
                  url: "/about",
                },
                {
                  label: <MyComponent4 />,
                  icon: "fa fa-male",
                  url: "/student",
                },
                {
                  label: <MyComponent5 />,
                  icon: "fa fa-male",
                  url: "/coordonator",
                },
                {
                  label: <MyComponent6 />,
                  icon: "fa fa-phone",
                  url: "/contact",
                },
                {
                  label: <MyComponent7 />,
                  icon: "fa fa-globe",
                  url: "/language",
                },
              ]}
            ></Menubar>
          </div>
        </div>
      </div>
    );
  }
}
export default Nav;
