import * as React from "react";
import "../../src/css/Header.css";
import { useTranslation } from "react-i18next";
import { Button } from "primereact/button";
import { withRouter } from "react-router-dom";
import axios from "axios";
function MyComponent() {
  const { t } = useTranslation();
  return <strong>{t("header.1")}</strong>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <strong>{t("header.2")}</strong>;
}
class Header extends React.Component {
  constructor() {
    super();
    // this.logout = this.logout.bind(this);
    this.state = {};
  }

  logout() {
    axios
      .post("http://localhost:8000/api/logout")
      .then((response) => {
        this.props.history.push(`/login`);
        sessionStorage.removeItem("email");
        sessionStorage.removeItem("name");
        sessionStorage.removeItem("id");
        sessionStorage.removeItem("poza");
        window.location.reload(false);
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  redirectToTarget = () => {
    this.props.history.push(`/login`);
  };
  redirectToTarget2 = () => {
    this.props.history.push(`/profile`);
  };
  render() {
    var loggedUser = sessionStorage.getItem("email") ? true : false;
    return (
      <div>
        <div className="flex-container">
          {loggedUser === true ? (
            <>
              <Button
                label="Logout"
                onClick={(e) => {
                  this.logout(e);
                }}
                icon="pi pi-power-off"
                style={{ marginLeft: 4 }}
              />
              <Button
                label="Profile"
                onClick={this.redirectToTarget2}
                icon="fa fa-male"
                style={{ marginLeft: 2 }}
              />
            </>
          ) : (
            <Button
              label="Login"
              onClick={this.redirectToTarget}
              icon="fa fa-sign-in"
              style={{ marginLeft: 4 }}
            />
          )}
        </div>
        <div className="header-style">
          <header>
            <MyComponent></MyComponent> <br />
            <MyComponent2></MyComponent2>
          </header>
        </div>
      </div>
    );
  }
}
export default withRouter(Header);
