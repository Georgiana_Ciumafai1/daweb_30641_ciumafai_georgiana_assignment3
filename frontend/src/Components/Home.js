import * as React from "react";
import signup from "../img/download.png";
import signup2 from "../img/py.png";
import signup3 from "../img/CLIPS.png";
import { Grid, Cell } from "react-mdl";
import "../css/Home.css";
import { useTranslation } from "react-i18next";

function Home() {
  const { t } = useTranslation();

  return (
    <div style={{ width: "100%", margin: "auto" }}>
      <Grid className="landing-grid">
        <Cell col={2}>
          <div className="banner-text">
            <p>{t("home.1")}</p>
          </div>
        </Cell>
        <Cell col={2}>
          <div className="banner-text">
            <p>{t("home.2")}</p>
          </div>
        </Cell>
        <Cell col={2}>
          <div className="banner-text">
            <img src={signup} alt="signup" className="avatar-img" />
            <hr></hr>
            <img src={signup2} alt="signup2" className="avatar-img" />
            <img src={signup3} alt="signup3" className="avatar-img" />
            <br />
            <br />
          </div>
        </Cell>
      </Grid>
    </div>
  );
}

export default Home;
