import * as React from "react";
import "../css/Student.css";
import { FileUpload } from "primereact/fileupload";

import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Message } from "primereact/components/message/Message";
import { Grid, Cell } from "react-mdl";
import "../css/Profile.css";
import axios from "axios";
import FormData from "form-data";

import { ListBox } from "primereact/listbox";
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      domains: [],
      formModel: {
        name: "",
        email: "",
        password: "",
        domains: [],
      },
      errors: {
        name: "",
        email: "",
        password: "",
        domains: [],
      },
      isValid: false,
      user: {
        id: "",
        name: "",
        email: "",
        domains: "",
        email_verified_at: "",
        created_at: "",
        updated_at: "",
      },
    };
  }
  getUser() {
    var id = sessionStorage.getItem("id");
    console.log(id);
    axios
      .post("http://localhost:8000/api/getUser/?id=" + id, {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      })
      .then((response) => {
        this.setState({ user: response.data });
        //sessionStorage.setItem("email", this.state.email);
        console.log(response.data);
        console.log(this.state.user);
        //sessionStorage.setItem("name", response.data.name);
        // sessionStorage.setItem("name", response.data.user.name);
        //  this.props.history.push("/home");
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  componentDidMount() {
    //this.getUser();
  }
  submit() {
    var name = sessionStorage.getItem("name");
    var email = sessionStorage.getItem("email");

    var formModel = this.state.formModel;

    if (formModel.name === "") {
      let formModel = this.state.formModel;
      formModel.name = name;
      this.setState({ formModel: formModel });
    }
    if (formModel.email === "") {
      let formModel = this.state.formModel;
      formModel.email = email;
      this.setState({ formModel: formModel });
    }

    formModel.domains = this.state.domains;
    this.setState({ formModel: formModel });
    console.log(formModel);
    var id = sessionStorage.getItem("id");
    console.log(id);
    axios
      .post("http://localhost:8000/api/getUser/?id=" + id, formModel, {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      })
      .then((response) => {
        this.setState({ user: response.data });
        //sessionStorage.setItem("email", this.state.email);
        console.log(response.data);
        console.log(this.state.user);
        window.location.reload(false);
        //sessionStorage.setItem("name", response.data.name);
        // sessionStorage.setItem("name", response.data.user.name);
        //  this.props.history.push("/home");
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  fileSelectedHandler = (event) => {
    console.log(event.target.files[0]);
  };

  myUploader(event) {
    //event.files == files to upload
    console.log(event.files[0]);
    const fd = new FormData();
    fd.append("image", event.files[0], event.files[0].name);
    console.log(event.files[0].name);
    axios
      .post("http://localhost:8000/api/storeFile", fd, {
        headers: { "Content-Type": "multipart/form-data" },
        withCredentials: true,
      })
      .then((response) => {
        sessionStorage.setItem("poza", "ok");
        console.log(response);
        window.location.reload(false);
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  render() {
    const invoiceUploadHandler = ({ files }) => {
      const [file] = files;
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        uploadInvoice(e.target.result);
      };
      fileReader.readAsDataURL(file);
    };
    const uploadInvoice = (invoiceFile) => {};
    const domains = [
      { name: "Sport" },
      { name: "IT" },
      { name: "Music" },
      { name: "Movies" },
      { name: "Travell" },
      { name: "Acrobatics" },
      { name: "Astronomy" },
      { name: "Astrology" },
      { name: "Running" },
      { name: "Aquascaping" },
      { name: "Book" },
      { name: "Building" },
      { name: "Blogging" },
      { name: "Martial Art" },
      { name: "Acting" },
    ];
    var name = sessionStorage.getItem("name");
    //var email = sessionStorage.getItem("email");
    return (
      <div style={{ width: "100%", margin: "auto" }}>
        {" "}
        <Grid className="landing-grid">
          <Cell col={12}>
            <div className="banner-text">
              <h1>Welcome {name} !</h1>
              <h1> Here are you profile details. </h1>
              <h1> Press submit to make changes.</h1>
              <div className="wrapper">
                <div className="p-grid p-fluid">
                  <div className="p-col-12 p-md-4">
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">
                        <i className="pi pi-user"></i>
                      </span>
                      <InputText
                        placeholder={name}
                        onChange={(event) => {
                          let formModel = this.state.formModel;
                          formModel.name = event.target.value;
                          this.setState({ formModel: formModel });
                          sessionStorage.setItem("name", formModel.name);
                        }}
                      />
                    </div>
                  </div>
                  {this.state.errors.name !== "" ? (
                    <Message severity="warn" text={this.state.errors.name} />
                  ) : null}

                  <div className="p-col-12 p-md-4">
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">P</span>
                      <InputText
                        placeholder="Password"
                        onChange={(event) => {
                          let formModel = this.state.formModel;
                          formModel.password = event.target.value;
                          this.setState({ formModel: formModel });
                        }}
                      />
                    </div>
                    <h1>Choose domains of interests:</h1>
                    <ListBox
                      value={this.state.domains}
                      filter={true}
                      filterPlaceholder="Search"
                      options={domains}
                      onChange={(e) => this.setState({ domains: e.value })}
                      multiple={true}
                      optionLabel="name"
                      listStyle={{ maxHeight: "150px" }}
                    />
                    <br />
                  </div>
                </div>
                <Button
                  label="Submit"
                  className="p-button"
                  onClick={() => this.submit()}
                />
              </div>
              <br />
              <h1>Photo Upload</h1>
              <br />

              <FileUpload
                name="demo[]"
                url="./upload"
                mode="basic"
                customUpload={true}
                uploadHandler={this.myUploader}
              />
              <br />
              <br />
              <br />
            </div>
            <br />
            <br />
          </Cell>
        </Grid>
      </div>
    );
  }
}
export default Profile;
