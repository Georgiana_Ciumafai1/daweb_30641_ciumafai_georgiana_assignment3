import axios from "axios";

export class UserService {
  static root = "http://localhost:8000";

  static addUser(userModel) {
    return new Promise((resolve, reject) => {
      axios
        .post(this.root + "/api/register", userModel, {
          headers: { "Content-Type": "application/json" },
        })
        .then(
          (response) => {
            resolve(response);
          },
          (error) => {
            reject(error.response.data);
          }
        );
    });
  }

  static validateUser(userModel, errors) {
    let isValid = true;
    let expr1 = /^[0-9a-zA-z_-]+$/;

    if (userModel.password === "") {
      errors.password = "Parola nu poate fi vida";
      isValid = false;
    } else {
      errors.password = "";
    }

    if (userModel.name === "") {
      errors.name = "Nume nu poate fi vid";
      isValid = false;
    } else {
      errors.name = "";
    }

    expr1 = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (userModel.email === "" || userModel.email.match(expr1) === null) {
      errors.email = "E-mail invalid";
      isValid = false;
    } else {
      errors.email = "";
    }

    return isValid;
  }
}
