<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Comments;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;


class CommentsController extends Controller
{
    //
    public function postComment(Request $request)
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $u = User::findOrFail($userId);
            $nameu = $u->name;
            $data = $request->all();

            $comm = Comments::create([
                'id_user' => $userId,
                'comm' => $data['value'],
                'name' => $nameu,
            ]);

            return response()->json(['message' => 'Comentariu adaugat cu succes']);
        }
    }

    public function getComment()
    {

        $coms = DB::table('comments')->get();
        return response()->json($coms);
    }
}
