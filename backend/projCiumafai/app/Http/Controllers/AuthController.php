<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use  Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    protected $user;

    public function postLogin(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        $request->session()->put("data", $request->input());
        $value = $request->session()->get('key');
        if (Auth::attempt($credentials, true)) {
            // Authentication passed...

            return response()->json(auth()->user(), 200);
        }
        return response()->json('Oppes! You have entered invalid credentials');
    }

    public function postRegistration(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
        ]);

        $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        return response()->json(compact("user"));
    }
    public function getUser(Request $request)
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $u = User::findOrFail($userId);
            // $user = Auth::user();
            // $email = Auth::user()->email;
            // $name = Auth::user()->name;

            $u->fill([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'domains' => $request->domains
            ]);
            $u->save();
            return response()->json(compact("u"));
        }
        //  dd($user);
        // error_log(auth()->user());
        // error_log($request->all());
        //error_log($u);

    }

    public function updateUser(Request $id, $name, $email, $password)
    {

        $u = User::findOrFail($id);
        //$bodyContent = $request->getContent();
        // Fill user model

        $u->fill([
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);

        // Save user to database
        // $user->save();
        //$u->save();
        return response()->json(compact("u"));
    }



    public function logout()
    {
        Session::flush();
        Auth::logout();
        return response()->json("logout");
    }
}
