<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('register', 'AuthController@postRegistration');
//Route::post('login', 'AuthController@postLogin');

//Route::post('login', 'AuthController@postLogin');
Route::post('register', 'AuthController@postRegistration');
Route::post('logout', 'AuthController@logout');
Route::post('storeFile', 'FileController@storeFile');
Route::post('postComment', 'CommentsController@postComment');
Route::get('getComment', 'CommentsController@getComment');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['web']], function () {
    // your routes here

    Route::post('login', 'AuthController@postLogin');
    Route::post('getUser', 'AuthController@getUser');
    Route::post('updateUser', 'AuthController@updateUser');
    Route::post('/uploadfile', 'UploadFileController@showUploadFile');
});
